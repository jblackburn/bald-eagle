Obstacle = function(context) {
	this.x = canvas.width + 10;
	this.y = Math.floor(Math.random() * (canvas.height) + 1);
	this.width = 20;
	this.height = 20;
	this.speed = 5;
	this.dead = false;

	Obstacle.prototype.getCoords = function() {
    	return {x:this.x, y:this.y};
	}

	Obstacle.prototype.getWidthHeight = function() {
		return {width:this.width, height:this.height};
	}

	Obstacle.prototype.kill = function() {
		this.dead = true;
		delete this;
	}

	Obstacle.prototype.draw = function() {

		if(!this.dead) {
			context.fillStyle = "#000000";
			context.fillRect(this.x, this.y, this.width, this.height);

			this.x -= this.speed;
		}
	}
}