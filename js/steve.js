Steve = function(eagle) {

	/*
	Steve is my collision checker, there are many others like him. But he is mine. 
	*/

	var collisionRadius = 25;
	
	Steve.prototype.checkCollision = function(obstacle) {

		var obstacleCoords = obstacle.getCoords();
		var obstacleWidthHeight = obstacle.getWidthHeight();
		var eagleCoords = eagle.getCoords();
		var eagleWidthHeight = eagle.getWidthHeight();

		var eagleCenter = {x:(eagleCoords.x + (eagleWidthHeight.width / 2)), y:(eagleCoords.y - (eagleWidthHeight.height / 2))};
		var obstacleCenter = {x:(obstacleCoords.x + (obstacleWidthHeight.width / 2)), y:(obstacleCoords.y - (obstacleWidthHeight.height / 2))};

		var dist = Math.sqrt(Math.pow((eagleCenter.x - obstacleCenter.x), 2) + Math.pow((eagleCoords.y - obstacleCenter.y), 2));

		return dist <= collisionRadius;
	
	}

}