BG = function(document, canvas, context) {

	this.colour1 = "#006D7C";
	this.colour2 = "#F0E0C2";
	this.colour3 = "#EFAD8A";

	this.cloudArray = new Array();
	this.maxClouds = 10;
	this.cloudSpawnChance = 0.01;

	for(var i = 0; i < Math.floor(Math.random() * 5) + 1; i++) {
		this.cloudArray.push(new Cloud(document, canvas, context));
	}

	BG.prototype.draw = function() {
		var grd = context.createLinearGradient(0,0,0,canvas.height);
		grd.addColorStop(0, this.colour1);
		grd.addColorStop(0.75, this.colour2);
		grd.addColorStop(1, this.colour3);

		context.fillStyle = grd;
		context.fillRect(0, 0, canvas.width, canvas.height);

		if(this.cloudArray.length < this.maxClouds && Math.random() <= this.cloudSpawnChance) {
		    this.cloudArray.push(new Cloud(document, canvas, context));
		}

		for (var i = 0; i < this.cloudArray.length; i++)
      	{ 
	      this.cloudArray[i].draw();

	      if(this.cloudArray[i].getCoords().x < -350) {
	        this.cloudArray[i].kill();

	        var removeItem = this.cloudArray.indexOf(this.cloudArray[i]);
         	if(removeItem != -1) this.cloudArray.splice(removeItem, 1);
	      } 
	    }
	}
}