Cloud = function(document, canvas, context) {
	this.x = canvas.width + 350;
	this.y = Math.floor(Math.random() * canvas.height / 2) + 1;
	this.speed = Math.floor(Math.random() * 7) + 1;
	this.dead = false;

	this.image = document.createElement('img');
    this.image.src = "img/cloud" + (Math.floor(Math.random() * 6) + 1) + ".png";

    Cloud.prototype.kill = function() {
		this.dead = true;
		delete this;
	}

	Cloud.prototype.getCoords = function() {
		return {x: this.x, y:this.y};
	}

	Cloud.prototype.draw = function() {

		if(!this.dead) {
			context.drawImage(this.image, this.x, this.y);

			this.x -= this.speed;
		}

	}
}