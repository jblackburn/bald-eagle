Eagle = function(document, canvas, context) {

	this.lift = 15;
	this.canFlap = true;
	this.maxFallSpeed = 5;
	this.velY = 0;
	this.y = 100;
	this.x = 100;
	this.width = 62; //This shouldn't be a magic number
	this.height = 40; //This shouldn't be a magic number

	this.image = document.createElement('img');
    this.image.src = "img/eagle.png";

    //this.width = this.image.width;
    //this.height = this.image.height;

    Eagle.prototype.flap = function() {
    	this.velY -= this.lift;
    };

    Eagle.prototype.getCoords = function() {
    	return {x:this.x, y:this.y};
    }

    Eagle.prototype.getWidthHeight = function() {
    	return {width:this.width, height:this.height};
    }

    Eagle.prototype.getCanFlap = function() {
    	return this.canFlap;
    }

    Eagle.prototype.setCanFlap = function(value) {
    	this.canFlap = value;
    }

    Eagle.prototype.draw = function(gravity) {

   	  	context.drawImage(this.image, this.x, this.y);

	  	if(this.velY < this.maxFallSpeed) this.velY += gravity;

	  	if(Math.abs(this.velY) > Math.abs(this.lift)) this.velY = -this.lift;

	  	this.y += this.velY;

	  	if(this.y > canvas.height) this.y = 1;
	  	if(this.y < 0) this.y = canvas.height - 1;

    }
}